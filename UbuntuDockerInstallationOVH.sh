#!/bin/sh

set -e

#
# Installing OVH RTM Tools
#
touch /etc/apt/sources.list.d/rtm.list;
printf 'deb http://last.public.ovh.metrics.snap.mirrors.ovh.net/ubuntu bionic main\ndeb http://last.public.ovh.rtm.snap.mirrors.ovh.net/ubuntu bionic main' >> /etc/apt/sources.list.d/rtm.list;
curl  https://last-public-ovh-rtm.snap.mirrors.ovh.net/ovh_rtm.pub | apt-key add -;
curl  http://last.public.ovh.metrics.snap.mirrors.ovh.net/pub.key | apt-key add -;
apt-get update ;
apt-get install -y ovh-rtm-metrics-toolkit;


#
# Installing Docker
#
curl -fsSL https://get.docker.com/ | sh -;