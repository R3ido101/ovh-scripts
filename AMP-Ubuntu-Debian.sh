#!/bin/sh

exec >> $HOME/AMP-install.log
exec 2>&1
set -e

#
# Added a AMP user to AMP Will run.
#

useradd -d /home/AMP -m AMP -s /bin/bash;

#
# Installing AMP repo to aquire AMP
#

apt-get install -y software-properties-common dirmngr apt-transport-https;
apt-key adv --fetch-keys http://repo.cubecoders.com/archive.key;
apt-add-repository -y "deb http://repo.cubecoders.com/ debian/";
apt update -y;

#
# Installing Java
#

apt install -y openjdk-8-jre-headless;

#
# Actually installing AMP
#

apt install -y ampinstmgr

#
# Add ampinstmgr to cron
#
su -l AMP;
(crontab -l ; echo "@reboot /opt/cubecoders/amp/ampinstmgr -b")| crontab -;
exit;