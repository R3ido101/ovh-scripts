#!/bin/bash

#
# Get Pulseway From Pulseway.com and install it
#
wget http://www.pulseway.com/download/pulseway_x64.deb;
dpkg -i pulseway_x64.deb;
cp /etc/pulseway/config.xml.sample /etc/pulseway/config.xml;