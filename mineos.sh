#!/bin/sh
exec >> $HOME/install.log
exec 2>&1
set -e

#Dependencies 
apt install -y curl
curl -sL https://deb.nodesource.com/setup_8.x | bash -

# update repositories
apt-get update

# More Depts

apt-get install -y git supervisor rdiff-backup screen build-essential nodejs

useradd -d /usr/games -m games -s /bin/nologin;

# download the necessary prerequisite components for mineos
apt-get -y install screen python-pip rdiff-backup git openjdk-8-jre-headless
pip2 install cherrypy==3.2.3

# download the most recent mineos web-ui files from github
cd /usr/games
git clone git://github.com/hexparrot/mineos minecraft
cd minecraft
git config core.filemode false
chmod +x server.py mineos_console.py generate-sslcert.sh
ln -s /usr/games/minecraft/mineos_console.py /usr/local/bin/mineos

# distribute service related files
cd /usr/games/minecraft
cp init/mineos /etc/init.d/
chmod 744 /etc/init.d/mineos
update-rc.d mineos defaults
cp init/minecraft /etc/init.d/
chmod 744 /etc/init.d/minecraft
update-rc.d minecraft defaults
cp mineos.conf /etc/

# generate self-signed certificate
./generate-sslcert.sh

# start the background service
service mineos start