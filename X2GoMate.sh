#!/bin/bash

set -e

# Get PPA

apt-get install software-properties-common -y;

add-apt-repository ppa:x2go/stable -y;

# Get Mate desktop and x2goserver

apt-get install x2goserver x2goserver-xsession mate-core mate-desktop-environment -y;