#!/bin/sh

yum -y update;

yum install -y java-1.8.0-openjdk tmux socat unzip git wget;

useradd -d /home/AMP -m AMP -s /bin/bash -G tty;

mkdir -p /opt/cubecoders/amp;

cd /opt/cubecoders/amp;

wget http://cubecoders.com/Downloads/ampinstmgr.zip;

unzip ampinstmgr.zip;

rm -f ampinstmgr.zip;

ln -sf /opt/cubecoders/amp/ampinstmgr /usr/bin/ampinstmgr;
